# Experiència amb els visors

![](activitat2.images/llistat_visors.png)


## Objectius

Introduir al professorat els diferents **visors** que permeten l'accés als escriptoris virtuals i avaluar-ne les limitacions i els rendiments.


## Índex de continguts

- Tipus de visors i ús
- Instal·lació de visors nadius
- Connexió de dispositius USB
- Enquesta d'experiència d'usuari amb cada visor


## Metodologia

El curs es realitza a [PilotFP](https://pilotfp.gencat.isardvdi.com/login). L’alumne (docent inscrit) és el centre de l’aprenentatge. Aquest aprenentatge es construirà a partir de la pràctica proposada, assolint les competències relacionades amb les metodologies actives mitjançant el feedback.


## Desenvolupament

### 1. Instal·lació

Per fer ús del visor SPICE i del visor RDP, s'hauran d'instal·lar a l'equip amfitrió els programes respectius que precisa cada visor per poder funcionar. 

1. Instal·lar programa per visor SPICE
2. Instal·lar programa per visor RDP (si l'equip del docent és Windows, no fa falta instal·lar res)

!!! Documentació
    [Descàrrega directa de programari per a visors](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers.ca/#descarrega-directa-de-programari)


### 2. Utilitzar els visors

L'elecció dels visors dependrà del sistema operatiu de l'escriptori virtual.

En la creació de l'escriptori o en la seva posterior edició, es seleccionen els visors pels quals es vol poder accedir a l'escriptori arrencat. 

1. Crear un escriptori que tingui sistema operatiu Windows, seleccionant tots els visors i afegint la interfície de xarxa **Wireguard VPN**
2. Crear un escriptori que tingui sistema operatiu Linux (Ubuntu, Debian o Fedora), seleccionant tots els visors i afegint la interfície de xarxa **Wireguard VPN**
3. Evaluar-ne el rendiment segons el sistema operatiu de l'escriptori

!!! Documentació
    [Com utilitzar el visor SPICE](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers.ca/#com-utilitzar-el-visor-spice)

    [Com utilitzar el visor al navegador](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers.ca/#com-utilitzar-el-visor-al-navegador)

    [Com utilitzar els visors RDP i RDP VPN](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers.ca/#com-utilitzar-els-visors-rdp-i-rdp-vpn)


### 3. Escanejar dispositius USB

Amb els visors **SPICE** i **RDP** es poden escanejar dispositius USB des de l'equip amfitrió fins a l'escriptori virtual.

1. Escanejar un dispositiu USB amb visor SPICE i comprovar que apareix a l'escriptori
2. Escanejar un dispositiu USB amb visor RDP o RDP VPN i comprovar que apareix a l'escriptori

!!! Documentació
    [Escanejar USB amb SPICE](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers.ca/#escanejar-dispositius-usb)

    [Escanejar USB amb RDP](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers.ca/#escanejar-dispositius-usb_1)


## Avaluació

S'avaluarà la realització de l'enquesta d'experiència d'usuari amb els visors.