# Usuaris i grups

![](activitat6.images/users-management.png){width="30%"}
![](activitat6.images/users1.png){width="80%"}


## Objectius

Introduir al gestor la creació i gestió dels usuaris i grups a la plataforma, així com gestionar els seus mètodes de sessió. 

Aquestes gestions es realitzen a la part **Users - Management** de l'apartat d'**Administració**.


## Índex de continguts

- Com es creen grups
- Com es creen usuaris (individual i de forma massiva per arxiu CSV)
- Clau d'autoregistre
- Què són els grups secundaris?
- Què són els grups enllaçats?


## Metodologia

El curs es realitza a [PilotFP](https://pilotfp.gencat.isardvdi.com/login). L’alumne (docent gestor inscrit) és el centre de l’aprenentatge. Aquest aprenentatge es construirà a partir de la pràctica proposada, assolint les competències relacionades amb les metodologies actives mitjançant el feedback.


## Desenvolupament

### 1. Crear un grup amb usuaris

1. Crea un grup amb nom **grupC**
2. Crea un grup amb nom **professorat**
3. Crea un usuari docent amb nom **docent03** de forma individual, mitjançant botó ![](activitat6.images/add_new_user.png), al grup **professorat**
4. Crear 5 alumnes amb noms **alumneC01, alumneC02, alumneC03, alumneC04 i alumneC05** de forma massiva per arxiu CSV, mitjançant botó ![](activitat6.images/bulk_create_users.png), al grup **grupC**

!!! Documentació
    [Com crear un grup](https://isard.gitlab.io/isardvdi-docs/manager/manage_user.ca/#creacio-de-grups)

    [Com crear un usuari de forma individual](https://isard.gitlab.io/isardvdi-docs/manager/manage_user.ca/#individualment)

    [Com crear usuaris de forma massiva](https://isard.gitlab.io/isardvdi-docs/manager/manage_user.ca/#creacio-massiva)


### 2. Clau d'autoregistre

Per tal que els usuaris puguin autoregistrar-se a la plataforma sense que hagem de crear els usuaris, aquest pot crear una clau d'autoregistre per grup i rol. L'usuari en validar-se al sistema per primera vegada li demanarà el codi de registre que li hem proporcionat. L'usuari podrà utilitzar qualsevol sistema d'autenticació actiu (SAML/idalu, Google).

1. Crear clau d'autoregistre pels usuaris amb rol **user** al grup **grupC**
2. En una pestanya de navegació privada, iniciar sessió per Google o SAML amb un compte de prova, escrivint la clau d'autoregistre de **user**
3. Refrescar la vista d'Administració **Users - Management** i comprovar que s'ha creat un usuari nou pel grup **grupC**

Un mateix grup pot tenir fins a 3 claus d'autoregistre, cadascuna per cadascun dels rols disponibles **user**, **advanced** i **manager**. L'usuari gestor pot compartir cadascuna de les claus d'autoregistre amb cadascun dels usuaris del centre que es vulgui tinguin un usuari a la plataforma, per evitar crear aquests usuaris a mà (s'ha de tenir en compte que el mètode d'inici de sessió que escullin, Google o SAML, serà el mètode d'inici de sessió que hauran de fer servir sempre per loguejar-se amb aquest usuari).

Quan ja no es desitja que es pugui fer servir el codi de registre, podrem tornar al formulari on el vam generar i esborrar-lo desactivant la casella.

!!! Documentació
    [Com crear una clau d'autoregistre](https://isard.gitlab.io/isardvdi-docs/manager/manage_user.ca/#clau-dautoregistre)


### 3. Grups secundaris

S'utilitzen els grups secundaris per poder compartir recursos entre grups i per crear desplegaments, de manera més granular.

1. Editar l'usuari **docent03** i ficar-li de grup secundari el grup **grupC**
2. Anar al perfil d'usuari amb l'usuari gestor, duplicar una plantilla per fer-la seva amb nom **Plantilla per grupC** i compartir-la amb el grup **grupC**
3. Impersonar-se com qualsevol usuari del grup **grupC**
    1. Anar a crear un nou escriptori i veure com li surt la plantilla
4. Impersonar-se com l'usuari **docent03**
    1. Anar a crear un nou escriptori amb qualsevol nom i veure com li surt la plantilla
    2. Anar a crear un nou desplegament pel grup amb qualsevol nom **grupC** i veure com s'afegeix al desplegament un escriptori per l'usuari **docent03**
  
Editar l'usuari **docent03** i afegir-li com a grup secundari **grupC**, implica que a l'usuari **docent03** li surtin com compartits amb ell els recursos compartits amb el grup **grupC** (plantilles, mitjans, interfícies de xarxa, entre d'altres), i que se li creïn desplegaments conforme es creen pel grup **grupC**.

Tal com diu a la documentació oficial sobre el camp "**Secondary groups**", "*Tots els recursos que han estat compartits amb els seus grups secundaris s'heretaran automàticament per l'usuari creat. A més, l'usuari serà afegit a tots els desplegaments creats a qualsevol dels seus grups secundaris.*"

!!! Documentació
    [Com editar un usuari](https://isard.gitlab.io/isardvdi-docs/manager/manage_user.ca/#edicio-dusuaris)

    [Com impersonar-se com un altre usuari](https://isard.gitlab.io/isardvdi-docs/manager/manage_user.ca/#suplantar-usuari)

    [Com duplicar una plantilla](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#duplicar-plantilla)


### 4. Grups enllaçats

S'utilitzen els grups enllaçats per a poder compartir recursos entre grups sense necessitat de compartir-los amb tots.

1. Editar el grup **professorat** i enllaçar el grup amb **grupC**
2. Impersonar-se com qualsevol usuari del grup **grupC**
    1. Anar a crear un nou escriptori amb qualsevol nom i veure com li surt la plantilla **Plantilla per grupC**
3. Impersonar-se com qualsevol usuari del grup **professorat**
    1. Anar a crear un nou escriptori amb qualsevol nom i veure com li surt la plantilla **Plantilla per grupC**

Editar el grup **professorat** i afegir el grup enllaçat **grupC**, implica que a **professorat** li surtin com compartits amb els seus usuaris, els recursos compartits amb el grup **grupC** (plantilles, mitjans i interfícies de xarxa, entre d'altres).

Tal com diu a la documentació oficial sobre el camp "**Linked groups**", "*Tots els recursos compartits amb els seus grups enllaçats s'heretaran automàticament pel grup.*"

!!! Documentació
    [Com editar un grup](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#editar)

    [Com impersonar-se com un altre usuari](https://isard.gitlab.io/isardvdi-docs/manager/manage_user.ca/#suplantar-usuari)

    [Com duplicar una plantilla](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#duplicar-plantilla)


## Avaluació

S'avaluarà la realització de l'enquesta sobre els continguts d'aquest exercici.
