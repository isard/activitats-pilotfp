# Introducció

En aquest web hi trobareu les activitats proposades al curs de PilotFP.

- ACTIVITAT 1: [Gestió de desplegaments](activitat1/desplegaments.md)
- ACTIVITAT 2: [Experiència amb els visors](activitat2/visors.md)
- ACTIVITAT 3: [Gestió de plantilles](activitat3/plantilles.md)
- ACTIVITAT 4: [Laboratori client - servidor](activitat4/client_servidor.md)
- ACTIVITAT 5: [Gestió de recursos de la categoria](activitat5/recursos_categoria.md)
- ACTIVITAT 6: [Usuaris i grups](activitat6/grups_i_usuaris.md)
- ACTIVITAT 7: [Quotes i límits](activitat7/quotes_limits.md)
