# Quotes i límits

![](activitat7.images/quotas_limits.png){width="30%"}
![](activitat7.images/users-quotas_limits.png){width="80%"}


## Objectius

Introduir al gestor la creació i gestió de les quotes i límits d'usuaris i grups a la plataforma.

Aquestes gestions es realitzen a la part **Users - Quotas / Limits** de l'apartat d'**Administració**.

## Índex de continguts

- Com s'assginen les quotes a usuaris i grups
- Com s'assignen els límits a usuaris i grups


## Metodologia

El curs es realitza a [PilotFP](https://pilotfp.gencat.isardvdi.com/login). L’alumne (docent gestor inscrit) és el centre de l’aprenentatge. Aquest aprenentatge es construirà a partir de la pràctica proposada, assolint les competències relacionades amb les metodologies actives mitjançant el feedback.


## Desenvolupament

### 1. Quotes

Les quotes serveixen per limitar els recursos a nivell d'usuari individual, les quals es poden assignar a grups i usuaris.

1. Crear les quotes següents pel grup **grupA** de la teva categoria pels usuaris amb rol **user**:
    - Desktops: 3
    - Concurrent: 2
    - vCPUs: 8
    - Memory: 16
    - Total size: 300
    - Soft size: 250

    ![](activitat7.images/alumnes_quota.png){width="80%"}

2. Impersonar-se com a qualsevol alumne de **grupA** i:
    1. Crear 3 escriptoris amb noms qualsevol amb 4 vCPUs i 8 GB de memòria cadascun. Provar a crear un quart escriptori.
    2. Arrencar 2 escriptoris. Provar a arrencar el tercer. 

3. Crear les quotes següents per l'usuari **alumneA05**:
    - Desktops: 2
    - Concurrent: 1
    - vCPUs: 4
    - Memory: 8
    - Total size: 300
    - Soft size: 250

    ![](activitat7.images/alumnea05_quota.png){width="80%"}

4. Impersonar-se com l'usuari **alumneA05** i:
    1. Crear 2 escriptoris amb noms qualsevol amb 4 vCPUs i 8 GB de memòria cadascun. Provar a crear un tercer escriptori.
    2. Arrencar 1 escriptori. Provar a arrencar el segon.

Amb això es pot comprovar que un grup pot tenir unes quotes assignades, però usuaris individuals d'aquest grup poden tenir quotes personalitzades. En aquest cas les hem assignat de més petites (més restrictives), però les quotes personalitzades poden ser més grans (menys restrictives) que les aplicades al grup o a la categoria.

!!! Documentació
    [Definició de les diferents quotes de recursos](https://isard.gitlab.io/isardvdi-docs/manager/quotas_limits.ca/#quotes)

    [Com assignar quotes a un grup](https://isard.gitlab.io/isardvdi-docs/manager/quotas_limits.ca/#quota-de-grup)

    [Com assignar quotes a un usuari](https://isard.gitlab.io/isardvdi-docs/manager/quotas_limits.ca/#quota-dusuari)

    [Com impersonar-se com un altre usuari](https://isard.gitlab.io/isardvdi-docs/manager/manage_user.ca/#suplantar-usuari)


## 2. Límits

Els límits serveixen per limitar els recursos a nivell de conjunt d'usuaris, els quals es poden assignar només a grups.

1. Crea els límits següents pel grup **docents** de la teva categoria pels usuaris amb rol **advanced**:
    - Desktops: 3
    - Concurrent: 2
    - vCPUs: 8
    - Memory: 16
    - Total size: 300
    - Soft size: 250

2. Impersonar-se com ambdós usuaris del grup **docents** i:
    1. Crear dos escriptoris amb noms qualsevol per a **docent01** amb 4 vCPUs i 8 GB de memòria cadascun
    2. Crear un escriptori amb nom qualsevol per a **docent02** amb 4 vCPUs i 8 GB de memòria. Provar de crear un segon escriptori.
    3. Arrencar els dos escriptoris de **docent01**
    4. Provar d'arrencar un escriptori de **docent02**

Amb això es pot comprovar que els límits assignats a un grup seran sobre el conjunt de recursos utilitzats per la totalitat d'usuaris del grup. L'usuari gestor només pot assignar límits als grups. En aquest cas hem assignat límits a un grup que hi és a una categoria sense límits. En el cas de què la categoria tingués límits, els límits del grup poden ser més grans (menys restrictius) que les aplicades al grup.

!!! Documentació
    [Com assignar límits a un grup](https://isard.gitlab.io/isardvdi-docs/manager/quotas_limits.ca/#gestionar-limits)

    [Com impersonar-se com un altre usuari](https://isard.gitlab.io/isardvdi-docs/manager/manage_user.ca/#suplantar-usuari)


## Avaluació

S'avaluarà la realització de l'enquesta sobre els continguts d'aquest exercici.
