# Gestió de plantilles

![](activitat3.images/vista_plantilles.png)

## Objectius

Introduir al professorat el concepte de **plantilles**, duplicats de plantilles i herència entre escriptoris i plantilles.

## Índex de continguts

- Què és una plantilla
- Dependència entre plantilles i escriptoris
- Compartició de plantilles
- Duplicats de plantilles


## Metodologia

El curs es realitza a [PilotFP](https://pilotfp.gencat.isardvdi.com/login). L’alumne (docent inscrit) és el centre de l’aprenentatge. Aquest aprenentatge es construirà a partir de la pràctica proposada, assolint les competències relacionades amb les metodologies actives mitjançant el feedback.


## Desenvolupament

### 1. Crear una plantilla

Una plantilla és el disc d'un escriptori on es conserva el seu estat d'emmagatzematge, per tal de poder ser replicat en més escriptoris nous, que seran exactament iguals a l'escriptori d'inici.

Una plantilla no es pot iniciar, ja què l'emmagatzematge del seu disc es troba congelat. Per tal de fer evolucionar una plantilla, actualment s'ha de crear un escriptori a partir d'ella, instal·lar o configurar-hi el es vulgui, i convertir-lo en una nova plantilla.

1. Crear un escriptori de nom **Escriptori base** a partir de qualsevol plantilla
2. Accedir a aquest escriptori i fer alguna modificació (canviar fons de pantalla, instal·lar programa)
3. Crear una plantilla amb no **Plantilla base** a partir d'aquest escriptori
4. Crear un escriptori de nom **Escriptori nou** en base a la nova plantilla i comprovar que s'han mantingut els canvis

!!! Warning
    En una pràctica real amb la categoria del centre, abans de procedir a crear una nova plantilla, s'ha d'assegurar que no es deixa cap rastre d'informació personal al sistema o als navegadors (historial de navegació, sessions iniciades).
    D'aquesta forma es manté la privadesa de l'usuari i s'estalvien possibles riscos pel què fa la informació privada.

!!! Documentació
    [Com crear un escriptori](https://isard.gitlab.io/isardvdi-docs/user/create_desktop.ca/#vista-principal)

    [Com accedir a un escriptori arrencat](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers.ca/)

    [Com crear una plantilla](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#crear)


### 2. Accions de la plantilla

Les diferents accions que es poden fer amb una plantilla són:

- **Editar**: permet canviar el seu nom, descripció, visors i altres aspectes del maquinari virtual. Els escriptoris que ja s'hagin creat, mantindran el maquinari virtual i visors antics.
- **Compartir**: permet establir quins grups o usuaris concrets podran veure aquesta plantilla al llistat de plantilles a l'hora de triar en crear un nou escriptori o desplegament.
- **Deshabilitar**: habilitar o deshabilitar la plantilla fa que aquesta sigui visible o no pels usuaris amb qui s'ha compartit. De fet, si és deshabilitada, no la podrà veure ningú (tampoc l'usuari que l'ha creat) al llistat de plantilles que es mostra quan es crea un escriptori o un desplegament.


#### 2.1 Editar la plantilles

- Canviar els paràmetres de plantilla que permeten l'edició

!!! Documentació
    [Com editar una plantilla](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#editar)

#### 2.2 Compartir la plantilla

- Compartir la plantilla amb usuaris i/o grups

!!! Documentació
    [Com compartir una plantilla](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#compartir)


#### 2.3 Habilitar / Deshabilitar

1. Deshabilitar una plantilla i veure com no surt al llistat de plantilles disponibles a l'hora de crear un escriptori o un desplegament
2. Habilitar la plantilla de nou i veure com ja torna a sortir al llistat de plantilles a l'hora de crear un escriptori o un desplegament

!!! Documentació
    [Com habilitar/deshabilitar una plantilla](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#fer-visibleinvisible)


### 3. Dependència entre plantilles i escriptoris

Per treballar amb escriptoris, plantilles, i per entendre per què és arriscat esborrar-los, és important conèixer, o al menys tenir una noció, sobre el funcionament per sota de la cadena de discos que es genera quan es deriven escriptoris de discos i viceversa.

- TEORIA: llegir l'apartat del manual sobre el funcionament de la dependència de plantilles al sistema.

!!! Documentació
    [Teoria sobre plantilles i discos](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/)

### 4. Duplicar una plantilla

Aquesta funcionalitat serà important en quant a usuaris docents i gestors, donat que, seran ells els què duplicaran les plantilles precreades a IsardVDI i les compartiran amb els usuaris alumnes perquè ells les puguin utilitzar per crear escriptoris.

1. Com un usuari docent, veure les plantilles compartides amb tu i duplicar-ne una
2. Anar a la vista de plantilles personals per veure que s'ha creat una igual on ara el docent és el propietari

!!! Documentació
    [Com duplicar una plantilla](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#duplicar-plantilla)


## Avaluació

S'avaluarà la realització de l'enquesta sobre els continguts d'aquest exercici.