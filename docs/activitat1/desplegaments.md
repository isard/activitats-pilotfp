# Gestió de desplegaments

![](activitat1.images/videowall.png)


## Objectius

Introduir al professorat la funcionalitat **desplegaments** d'escriptoris virtuals i les diferents accions i interaccions amb l'alumnat.


## Índex de continguts

- Creació de desplegament d'escriptoris pels alumnes.
- Mostrar o ocultar escriptoris del desplegament als alumnes.
- Afegir alumnes a un desplegament existent.
- Crear nou escriptori original del desplegament a un alumne.
- Interactuar simultàniament amb els escriptoris dels alumnes.
- Descarregar visors directes dels escriptoris del desplegament.
- Eliminar un desplegament.


## Metodologia

El curs es realitza a [PilotFP](https://pilotfp.gencat.isardvdi.com/login). L’alumne (docent inscrit) és el centre de l’aprenentatge. Aquest aprenentatge es construirà a partir de la pràctica proposada, assolint les competències relacionades amb les metodologies actives mitjançant el feedback.


## Desenvolupament

### 1. Crear un desplegament

Partint dels grups d'alumnes precreats pel docent, s'ha de crear un desplegament per un dels grups.

!!! Documentació
    [Com crear un desplegament](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#com-crear-desplegaments)


### 2. Fer visible o invisible

Per a dur a terme aquesta acció, s'hauran de tenir ambdues sessions del docent i d'un alumne del desplegament obertes alhora, per veure els canvis. 
Es pot iniciar sessió com docent en una finestra, i en una finestra privada, iniciar sessió com l'alumne. 
Si es pot fer servir Firefox, també es recomana utilitzar l'[extensió Multi-Container](https://addons.mozilla.org/ca/firefox/addon/multi-account-containers/) per poder tenir múltiples sessions obertes de forma simultània al navegador web separades per diferents pestanyes.

1. Des de la sessió del docent: canviar la visibilitat del desplegament
2. Des de la sessió de l'alumne: observar com apareix i desapareix l'escriptori del seu perfil d'usuari a la vista "Escriptoris"

!!! Documentació
    [Com canviar la visibilitat d'un desplegament](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#fer-visibleinvisible)

### 3. Recrear escriptoris

#### 3.1 Si teniu rol de docent

S'eliminen diferents escriptoris del desplegament de forma individual, i es recrea el desplegament per veure com tornen a aparèixer de nou.

#### 3.2 Si teniu rol de gestor

Des de l'apartat d'Administració, s'afegeixen nous usuaris al grup sobre el qual s'ha creat el desplegament.

Es torna al perfil d'usuari a la vista Desplegaments, i es recrea el desplegament per veure com apareixen nous escriptoris, un per cada usuari nou afegit.

!!! Documentació
    [Com recrear escriptoris](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#recrear-els-escriptoris)

    [Com crear usuaris](https://isard.gitlab.io/isardvdi-docs/manager/manage_user.ca/#creacio-dusuaris)


### 4. Videowall

Una de les funcionalitats que permet una millor interacció simultània amb l'alumne és la del *videowall*.

Per a dur a terme aquesta acció, s'hauran de tenir ambdues sessions del docent i d'un alumne del desplegament obertes alhora, per veure els canvis. 
Es pot iniciar sessió com docent en una finestra, i en una finestra privada, iniciar sessió com l'alumne. 
Si es pot fer servir Firefox, també es recomana utilitzar l'[extensió Multi-Container](https://addons.mozilla.org/ca/firefox/addon/multi-account-containers/) per poder tenir múltiples sessions obertes de forma simultània al navegador web separades per diferents pestanyes.

1. Des de la sessió del docent: obrir el videowall del desplegament
2. Des de la sessió de l'alumne: arrencar i accedir a l'escriptori creat i treballar amb ell
3. Des de la sessió del docent: veure en el videowall com s'actualitza en directe la vista de l'escriptori d'aquest alumne conforme treballa

!!! Documentació
    [Visors](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers.ca/)

    [Com accedir al videowall](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#videowall)


### 5. Visors directes

Com a docent es pot descarregar el fitxer CSV de tots els visors directes dels escriptoris del desplegament. Qualsevol persona amb un visor directe pot accedir a l'escriptori, sense necessitat d'estar validat ni registrat al sistema. 

Mentre el desplegament estigui **ocult**, **ningú podrà fer ús de l'enllaç directe**. En el moment que el desplegament sigui **visible**, els enllaços de visor directe tornaran a estar actius.

!!! Documentació
    [Què és un visor directe?](https://isard.gitlab.io/isardvdi-docs/advanced/direct_viewer.ca/#visor-directe)

    [Com descarregar CSV de visors directes](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#com-descarregar-se-el-fitxer-de-visors-directes)


### 6. Esborrar desplegament

Esborrar un desplegament no és reversible. Una vegada esborrat un desplegament, ja no es pot recuperar ni el desplegament ni els seus escriptoris.

1. Crear un desplegament amb el nom "Desplegament per esborrar"
2. Esborrar el desplegament

!!! Documentació
    [Com esborrar un desplegament](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#com-esborrar-un-desplegament)


## Avaluació

S'avaluarà al sistema que s'hagin creat els ítems descrits en cada apartat del desenvolupament.








